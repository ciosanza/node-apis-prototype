# Node APIs Prototype

It demostrate how to build a simple authentication server using a depencency injection container.

## Getting Started

### Prerequisities

_Note all prerequisities_

### Installation

```bash
$ npm install
```

## Development

_All things to related development at here_

### Environment

_The environment to development and deployment at here_

### Test

```bash
$ npm test
```

## Deloyment

_All steps to deploy service on server at here_

## API doc [optional]

### Build document
```bash
$ npm run apidoc
```

### View document

Restart server
```bash
$ npm start
```
Open browser and go to: [localhost:3000/apidoc](localhost:3000/apidoc)

### Credentials

Credentials - Authentication Login

**POST**

```
/login
```

Parameter

| Field | Type | Description | Note |
|:------|:-----|:------------|:-----|
| email | String | User email |  |
| password | String | User password |  |

Input

```json
{
  "email": "john@connor.net",
  "password": "123456"
}
```

Success 200

| Field | Type | Description | Note |
|:------|:-----|:------------|:-----|
| token | String | Token of authenticated User |  |

Success

```json
HTTP/1.1 200 Token
{
  "ok": true,
  "token": "xyz.abc.123.hfg"
}
```

Error 401

| Name | Description |
|:-----|:------------|
| Unauthorized | The 401 of invalid Email or Password. |

Authentication error

```json
HTTP/1.1 401 Unauthorized
{
  "ok": false,
  "error": "Invalid Email or Password"
}
```


### Status

API Status

**GET**

```
/
```

Success 200

| Name | Type |	Description |
|:-----|:-----|:------------|
| status | String | API Status' message |

Success

```json
HTTP/1.1 200 OK
{"status": "Node APIs Prototype"}
```

### User

The Apis working with user

**DEL**

Deletes an authenticated user

```
/user
```

Header

| Field | Type | Description | Note |
|:------|:-----|:------------|:-----|
| Authorization | String | Token of authenticated user |  |

```json
{"Authorization": "JWT xyz.abc.123.hgf"}
```

Success

```json
HTTP/1.1 204 No Content
```

Delete error

```json
HTTP/1.1 412 Precondition Failed
```

**POST**

Register a new user

```
/users
```

Parameter

| Field | Type | Description | Note |
|:------|:-----|:------------|:-----|
| name | String | User name |  |
| email | String | User email |  |
| password | String | User password |  |

Input

```json
{
  "name": "John Connor",
  "email": "john@connor.net",
  "password": "123456"
}
```

Success 200

| Name | Type |	Description |
|:-----|:-----|:------------|
| id | String | User id |
| name | String | User name |
| email | String | User email |
| password | String | User encrypted password |
| updatedAt | Date | Update's date |
| createdAt | Date | Register's date |

Success

```json
HTTP/1.1 200 OK
{
  "id": "58c22bcac043a70c5111ba3b",
  "username": "John Connor",
  "email": "john@connor.net",
  "password": "$2a$10$SK1B1",
  "updatedAt": "2016-02-10T15:20:11.700Z",
  "createdAt": "2016-02-10T15:29:11.700Z"
}
```

Register error

```json
HTTP/1.1 412 Precondition Failed
```

**GET**

Return the authenticated user's data

```
/user
```

Header

| Field | Type | Description | Note |
|:------|:-----|:------------|:-----|
| Authorization | String | Token of authenticated user |  |

Header

```json
{"Authorization": "JWT xyz.abc.123.hgf"}
```

Success 200

| Name | Type |	Description |
|:-----|:-----|:------------|
| id | String | User id |
| name | String | User name |
| email | String | User email |

Success

```json
HTTP/1.1 200 OK
{
  "id": 1,
  "name": "John Connor",
  "email": "john@connor.net"
}
```

Find error

```json
HTTP/1.1 412 Precondition Failed
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Author Name** - *Short description in the project*

See also the list of [contributors](CONTRIBUTING.md) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
