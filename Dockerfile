# Use Node v6.10.0 as the base image
FROM node:6.10.0

# Use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependences:
ADD package.json  /tmp/package.json
RUN cd /tmp && npm install
RUN mkdir -p /app && cp -a /tmp/node_modules /app

# From here we load our application's code in, therefore the previous Docker
# "layer" that been cached will be used if possible
# Add source to image in app
WORKDIR /app
ADD . /app

# Install dependencies
# RUN npm installa

# Expose our server port
EXPOSE 3000

# Run our app
CMD ["npm", "test"]
