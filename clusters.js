'use strict';

const cluster = require('cluster');
const os = require('os');

const CPUS = os.cpus();
if (cluster.isMaster) {
  CPUS.forEach(() => cluster.fork());
  cluster.on('listening', worker => {
    console.log('Cluster %d connected', worker.process.pid);
  });
  cluster.on('disconnect', worker => {
    console.log('Cluster %d disconnected', worker.process.pid);
  });
  cluster.on('exit', (worker, code) => {
    if (code !== 0 && !worker.exitedAfterDisconnect) {
      console.log('Cluster %d is dead', worker.process.pid);
      cluster.fork();
      // Ensure to starts a new cluster if an old one dies
    }
  });

  // Zero down time
  process.on('SIGUSR2', () => {
    console.log('Restarting workers');
    const workers = Object.keys(cluster.workers);
    console.log(workers);
    function restartWorker(idx) {
      if (idx >= workers.length) return;
      const worker = cluster.workers[workers[idx]];
      console.log(`Stopping worker: ${worker.process.pid}`);
      worker.on('disconnect', function () {
        console.log('Shutdown complete');
        if (!worker.suicide) return;
        const newWorker = cluster.fork();
        newWorker.on('listening', () => {
          console.log('Replacement worker online.');
          restartWorker(idx + 1);
        });
      });
      worker.disconnect();
    }
    restartWorker(0);
  });
} else {
  require('./src/index');
}
