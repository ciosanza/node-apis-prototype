'use strict';

const jwt = require('jwt-simple');
require('../helpers');

describe('Routes: Tasks', () => {
  const Users = app.diContainer.get('db').models.Users;
  const Tasks = app.diContainer.get('db').models.Tasks;
  const jwtSecret = app.diContainer.get('config').jwtSecret;
  let token;
  let fakeTask;

  beforeEach(done => {
    Users.remove({})
      .then(() => {
        return Users.create({
          username: 'John',
          email: 'john@mail.com',
          password: '123456',
        });
      })
      .then(user => {
        Tasks.remove({})
          .then(() => Tasks.create([
            {
              title: 'Work',
              _creator: user._id,
            },
            {
              title: 'Study',
              _creator: user._id,
            },
          ]).then(tasks => {
            fakeTask = tasks[0];
            token = jwt.encode({
              id: user._id.valueOf().toString(),
              email: user.email,
              expire: Date.now() + (1000 * 60 * 60),
            }, jwtSecret);
            done();
          }));
      });
  });

  describe('GET /tasks', () => {
    describe('status 200', () => {
      it('returns a list of tasks', done => {
        request.get('/tasks')
          .set('Authorization', `JWT ${token}`)
          .expect(200)
          .end((error, res) => {
            expect(res.body.tasks).to.have.length(2);
            expect(res.body.tasks[0].title).to.eql('Work');
            expect(res.body.tasks[1].title).to.eql('Study');
            done (error);
          });
      });
    });
  });

  describe('POST /tasks', () => {
    describe('status 201', () => {
      it('creates a new task', done => {
        request.post('/tasks')
          .set('Authorization', `JWT ${token}`)
          .send({title: 'Run',})
          .expect(201)
          .end((error, res) => {
            expect(res.body.task.title).to.eql('Run');
            expect(res.body.task.done).to.be.false;
            done(error);
          });
      });
    });
  });

  describe('GET /tasks/:id', () => {
    describe('status 200', () => {
      it('returns one task', done => {
        request.get(`/tasks/${fakeTask._id.valueOf().toString()}`)
          .set('Authorization', `JWT ${token}`)
          .expect(200)
          .end((error, res) => {
            expect(res.body.task.title).to.eql('Work');
            done(error);
          });
      });
    });
    describe('status 404', () => {
      it('throws error when task not exist', done => {
        request.get('/tasks/0')
          .set('Authorization', `JWT ${token}`)
          .expect(404)
          .end((error) => done(error));
      });
    });
  });

  describe('PUT /tasks/:id', () => {
    describe('status 204', () => {
      it('updates a task', done => {
        request.put(`/tasks/${fakeTask._id.valueOf().toString()}`)
          .set('Authorization', `JWT ${token}`)
          .send({
            title: 'Travel',
            done: true,
          })
          .expect(204)
          .end((error) => done(error));
      });
    });
    describe('status 404', () => {
      it('throws error when task not exist', done => {
        request.get('/tasks/0')
          .set('Authorization', `JWT ${token}`)
          .expect(404)
          .end((error) => done(error));
      });
    });
  });

  describe('DELETE /tasks/:id', () => {
    describe('status 204', () => {
      it('removes a task', done => {
        request.put(`/tasks/${fakeTask._id.valueOf().toString()}`)
          .set('Authorization', `JWT ${token}`)
          .expect(204)
          .end((error => done(error)));
      });
    });
    describe('status 404', () => {
      it('throws error when task not exist', done => {
        request.get('/tasks/0')
          .set('Authorization', `JWT ${token}`)
          .expect(404)
          .end((error) => done(error));
      });
    });
  });
});
