'use strict';
const jwt = require('jwt-simple');
require('../helpers');

describe('Routes: Users', () => {
  const Users = app.diContainer.get('db').models.Users;
  const jwtSecret = app.diContainer.get('config').jwtSecret;
  let token;
  beforeEach(done => {
    Users.remove({})
      .then(() => {
        return Users.create({
          username: 'John',
          email: 'john@mail.com',
          password: '123456',
        });
      })
      .then((user) => {
        token = jwt.encode({
          id: user._id.valueOf().toString(),
          email: user.email,
          expire: Date.now() + (1000 * 60 * 60),
        }, jwtSecret);
        done();
      });
  });
  describe('GET /user', () => {
    describe('status 200', () => {
      it('returns an authenticated user', done => {
        request.get('/user')
          .set('Authorization', `JWT ${token}`)
          .expect(200)
          .end((error, res) => {
            expect(res.body.ok).to.be.true;
            expect(res.body.user.username).to.eql('John');
            expect(res.body.user.email).to.eql('john@mail.com');
            done(error);
          });
      });
    });
  });
  describe('DELETE /user', () => {
    describe('status 204', () => {
      it('delete an authenticated user', done => {
        request.delete('/user')
          .set('Authorization', `JWT ${token}`)
          .expect(204)
          .end((error) => {
            done(error);
          });
      });
    });
  });
  describe('POST /users', () => {
    describe('status 201', () => {
      it('creates a new user', done => {
        request.post('/users')
          .send({
            username: 'Mary',
            email: 'mary@mail.com',
            password: '123456',
          })
          .expect(201)
          .end((error, res) => {
            expect(res.body.ok).to.be.true;
            expect(res.body.user.username).to.eql('Mary');
            expect(res.body.user.email).to.eql('mary@mail.com');
            done(error);
          });
      });
    });
  });
});
