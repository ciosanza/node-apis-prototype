'use strict';
require('../helpers');

describe('Routes: Index', () => {
  describe('GET /', () => {
    it('returns the API status', done => {
      request.get('/')
        .expect(200)
        .end((error, res) => {
          const expected = {status: 'Node APIs Prototype',};
          expect(res.body).to.eql(expected);
          done(error);
        });
    });
  });
});
