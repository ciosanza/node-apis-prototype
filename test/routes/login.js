'use strict';
const mongoose = require('mongoose');
require('../helpers');

describe('Routes: Login', () => {
  const Users = app.diContainer.get('db').models.Users;
  describe('POST /login', () => {
    beforeEach(done => {
      mongoose.Promise = global.Promise;
      Users.remove({})
        .then(() => {
          return Users.create({
            username: 'John',
            email: 'john@mail.com',
            password: '123456',
          });
        })
        .then(() => done());
    });
    describe('status 200', () => {
      it('returns authenticated user token', done => {
        request.post('/login')
          .send({
            email: 'john@mail.com',
            password: '123456',
          })
          .expect(200)
          .end((error, res) => {
            expect(res.body).to.include.key('token');
            done(error);
          });
      });
    });
    describe('status 401', () => {
      it('throws error when password is incorrect', done => {
        request.post('/login')
          .send({
            email: 'john@mail.com',
            password: 'WRONG PASSWORD',
          })
          .expect(401)
          .end((error) => {
            done(error);
          });
      });
      it('throws error when email not exist', done => {
        request.post('/login')
          .send({
            email: 'wrong@mail.com',
            password: '123456',
          })
          .expect(401)
          .end((error) => {
            done(error);
          });
      });
      it('throws error when email and password are blank', done => {
        request.post('/login')
          .expect(401)
          .end((error) => {
            done(error);
          });
      });
    });
  });
});
