'use strict';

const supertest = require('supertest');
const chai = require('chai');

const requireHelper = require('./require_helper');
const app = requireHelper('index');

global.app = app;
global.request = supertest(app);
global.expect = chai.expect;
