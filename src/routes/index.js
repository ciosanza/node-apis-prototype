'use strict';

const logger = require('../libs/logger.js');

module.exports = function (app) {
  /**
   * @api {get} / API Status
   * @apiGroup Status
   * @apiSuccess {String} status API Status' message
   * @apiSuccessExample {json} apiSuccess
   *  HTTP/1.1 200 OK
   *  {"status": "Node APIs Prototype"}
   */
  app.get('/', (req, res) => {
    logger.info (`Node APIs Prototype at ${process.pid}`);
    res.json({status: `Node APIs Prototype at ${process.pid}`,});
  });
};
