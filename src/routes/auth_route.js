'use strict';

module.exports = function (app, authController) {
  /**
   * @api {port} /login Authentication Login
   * @apiGroup Credentials
   * @apiParam {String} email User email
   * @apiParam {String} password User password
   * @apiParamExample {json} Input
   *  {
   *    "email": "john@connor.net",
   *    "password": "123456"
   *  }
   * @apiSuccess {String} token Token of authenticated User
   * @apiSuccessExample {json} Success
   *  HTTP/1.1 200 Token
   *  {
   *    "ok": true,
   *    "token": "xyz.abc.123.hfg"
   *  }
   * @apiError Unauthorized The <code>401</code> of invalid Email or Password.
   * @apiErrorExample {json} Authentication error
   *  HTTP/1.1 401 Unauthorized
   *  {
   *    "ok": false,
   *    "error": "Invalid Email or Password"
   *  }
   */
  app.post('/login', authController.login);
};
