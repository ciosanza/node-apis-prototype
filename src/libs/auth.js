'use strict';

const passport = require('passport');
const passportJwt = require('passport-jwt');

const Strategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;

module.exports = function (db, config) {
  const Users = db.models.Users;
  const cfg = config;
  const params = {
    secretOrKey: cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
  };
  const strategy = new Strategy(params, (payload, done) => {
    Users.findById(payload.id)
      .then(user => {
        if (user) {
          return done(null, {
            id: user._id.valueOf().toString(),
            email: user.email,
          });
        }
        return done(null, false);
      })
      .catch(error => done(error, null));
  });
  passport.use(strategy);
  return {
    initialize: () => {
      return passport.initialize();
    },
    authenticate: () => {
      return passport.authenticate('jwt', cfg.jwtSession);
    },
  };
};
