'use strict';
const mongoose = require('mongoose');
const users = require('../models/users');
const tasks = require('../models/tasks');

module.exports = function (config) {
  const dbase = {};
  dbase.models = {
    Users: users(),
    Tasks: tasks(),
  };

  dbase.connect = (callback) => {
    console.log (config.db.database);
    const connectionString = `mongodb://${config.db.host}:${config.db.port}/${config.db.database}`;
    console.log ('config.mongoConnection >' + connectionString);
    mongoose.Promise = global.Promise;
    mongoose.connect(connectionString, callback);
  };

  return dbase;
};
