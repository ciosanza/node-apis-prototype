'use strict';

const fnArgs = require('parse-fn-args');

module.exports = function () {
  const dependencies = {};
  const factories = {};
  const initializings = {};
  const diContainer = {};


  diContainer.factory = (name, factory) => {
    factories[name] = factory;
  };

  diContainer.register = (name, dep) => {
    dependencies[name] = dep;
  };

  diContainer.init = (name, init) => {
    initializings[name] = init;
  };

  diContainer.get = (name, init=false) => {
    if (!dependencies[name]) {
      if (init) {
        const init = initializings[name];
        dependencies[name] = init && diContainer.inject(init);
      } else {
        // if(factories.hasOwnProperty(name)) {
        //   const factory = factories[name];
        //   dependencies[name] = diContainer.inject(factory);
        //   if ( dependencies.hasOwnProperty(name) && !dependencies[name]) {
        //     throw new Error('Cannot find custom module: ' + name);
        //   }
        // }
        const factory = factories[name];
        dependencies[name] = factory && diContainer.inject(factory);
        if (!dependencies[name]) {
          throw new Error('Cannot find custom module: ' + name);
        }

      }
    }
    return dependencies[name];
  };

  diContainer.inject = (factory) => {
    const args = fnArgs(factory)
      .map(function(dependency) {
        return diContainer.get(dependency);
      });
    return factory.apply(null, args);
  };

  diContainer.setup = () => {
    for (const fac in factories) {
      diContainer.get(fac);
    }
    for (const init in initializings) {
      diContainer.get(init, true);
    }
  };

  return diContainer;
};
