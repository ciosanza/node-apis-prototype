'use strict';

module.exports = {
  db: {
    host: process.env.DATABASE_HOST || 'localhost',
    database: 'authen',
    user: '',
    password: '',
    port: 27017,
  },
  jwtSecret: 'N0deAP1$',
  jwtSession: { session: false, },
};
