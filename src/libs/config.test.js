'use strict';

module.exports = {
  db: {
    host: process.env.DATABASE_HOST || 'localhost',
    database: 'authentest',
    user: '',
    password: '',
    port: 27017,
  },
  jwtSecret: 'Athe$K-TEST',
  jwtSession: { session: false, },
};
