'use strict';

const https = require('https');
const fs = require('fs');

module.exports = function (app) {
  if (process.env.NODE_ENV !== 'test') {
    const credentials = {
      key: fs.readFileSync('node-apis-prototype.key', 'utf8'),
      cert: fs.readFileSync('node-apis-prototype.cert', 'utf8'),
    };

    app.diContainer.get('db').connect(() => {
      console.log('Connected to MongoDB');
      https.createServer(credentials, app).listen(app.get('port'), () => {
        console.log(`Node APIs Prototype - Port ${app.get('port')}`);
      });
    });
  } else {
    app.diContainer.get('db').connect(() => {
      console.log('Connected to MongoDB Test');
    });
  }
};
