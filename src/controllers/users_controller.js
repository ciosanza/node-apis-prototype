'use strict';

module.exports = function (usersService) {
  const usersController = {};

  usersController.findUser = (req, res) => {
    usersService.findUser(req.user.id, (error, result) => {
      if (error) {
        return res.status(412).send({
          ok: false,
          error: error.message,
        });
      }
      res.status(200).send({ok: true, user: result,});
    });
  };

  usersController.deleteUser = (req, res) => {
    usersService.deleteUser(req.user.id, (error) => {
      if (error) {
        return res.status(412).send({
          ok: false,
          error: error.message,
        });
      }
      res.sendStatus(204);
    });
  };

  usersController.createUser = (req, res) => {
    usersService.createUser(req.body, (error, result) => {
      if (error) {
        return res.status(412).send({
          ok: false,
          error: error.message,
        });
      }
      res.status(201).send({ok: true, user: result,});
    });
  };

  return usersController;
};
