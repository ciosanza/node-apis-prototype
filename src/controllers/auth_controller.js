'use strict';

module.exports = function (authService) {
  const authController = {};

  authController.login = (req, res) => {
    authService.login(req.body.email, req.body.password, (error, result) => {
      if (error) {
        return res.status(401).send({
          ok: false,
          error: 'Invalid username/password',
        });
      }
      res.status(200).send({ ok: true, token: result, });
    });
  };

  authController.checkToken = (req, res) => {
    authService.checkToken(req.query.token, (error, result) => {
      if (error) {
        return res.status(401).send({
          ok: false,
          error: 'Token is invalid or expired',
        });
      }
      res.status(200).send({ ok: true, user: result, });
    });
  };

  return authController;
};
