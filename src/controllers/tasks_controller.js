'use strict';

module.exports = function (tasksService) {
  const taskController = {};

  taskController.getTasksByCreator = (req, res) => {
    tasksService.getTasksByCreator(req.user.id, (error, result) => {
      if (error) {
        return res.status(412).send({
          ok: false,
          error: error.message,
        });
      }
      res.status(200).send({
        ok: true,
        tasks: result,
      });
    });
  };

  taskController.createTask = (req, res) => {
    tasksService.createTask(req.body, (error, result) => {
      if (error) {
        return res.status(412).send({
          ok: false,
          error: error.message,
        });
      }
      res.status(201).send({
        ok: true,
        task: result,
      });
    });
  };

  taskController.findTask = (req, res) => {
    tasksService.findTask(req.params.id, req.user.id, (error, result) => {
      if (error) {
        return res.status(412).send({
          ok: false,
          error: error.message,
        });
      }
      if (result === null) {
        return res.status(404).send({
          ok: false,
          error: 'Task not exists',
        });
      }
      res.status(200).send({
        ok: true,
        task: result,
      });
    });
  };

  taskController.editTask = (req, res) => {
    tasksService.editTask(req.params.id, req.body, (error, result) => {
      if (error) {
        return res.status(412).send({
          ok: false,
          error: error.message,
        });
      }
      if (result === null) {
        return res.status(404).send({
          ok: false,
          error: 'Task not exists',
        });
      }
      res.sendStatus(204);
    });
  };

  taskController.removeTask = (req, res) => {
    tasksService.removeTask(req.params.id, (error, result) => {
      if (error) {
        return res.status(412).send({
          ok: false,
          error: error.message,
        });
      }
      if (result === null) {
        return res.status(404).send({
          ok: false,
          error: 'Task not exists',
        });
      }
      res.sendStatus(204);
    });
  };

  return taskController;
};
