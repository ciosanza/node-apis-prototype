'use strict';

const jwt = require('jwt-simple');

module.exports = (db, config) => {
  const Users = db.models.Users;
  const authService = {};

  authService.login = function (email, password, callback) {
    Users.findOne({ email, })
      .then(user => {
        if (!user) return callback(new Error('user is not exist'));
        user.comparePassword(password, (error, isMatch) => {
          if (error) return callback(error);
          if (!isMatch) return callback(new Error('Invalid password'));

          const token = jwt.encode({
            id: user._id.valueOf().toString(),
            email: email,
            expire: Date.now() + (1000 * 60 * 60), // 1 Hour
          }, config.jwtSecret);

          callback(null, token);
        });
      })
      .catch(error => callback(error));
  };

  // authService.checkToken = (token, callback) => {
  //   let userData;
  //   try {
  //     //jwt.decode will throw if the token is Invalid
  //     userData = jwt.decode(token, tokenSecret);
  //     if (userData.expire <= Date.now()) {
  //       throw new Error('Token expired');
  //     }
  //   } catch (error) {
  //     return process.nextTick(callback.bind(null, error));
  //   }

  //   users.findOne({ username: userData.username }, (error, user) => {
  //     if (error) return callback(error);
  //     callback(null, { username: userData.username });
  //   });
  // };

  return authService;
};
