'use strict';

const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

module.exports = function (db) {
  const Tasks = db.models.Tasks;
  const tasksService = {};

  tasksService.findTask = function (id, creatorId, callback) {
    if (!ObjectId.isValid(id)) return callback(null, null);
    Tasks.findOne({_id: id, _creator: creatorId,})
      .then(task => callback(null, task))
      .catch(error => callback(error));
  };

  tasksService.editTask = function (id, data, callback) {
    if (!ObjectId.isValid(id)) return callback(null, null);
    Tasks.findOneAndUpdate({_id: id,}, data)
      .then(() => callback(null))
      .catch((error) => callback(error));
  };

  tasksService.removeTask = (id, callback) => {
    if (!ObjectId.isValid(id)) return callback(null, null);
    Tasks.findOneAndRemove({_id: id,})
      .then(() => callback(null))
      .catch(error => callback(error));
  };

  tasksService.getTasksByCreator = (creatorId, callback) => {
    Tasks.find({_creator: creatorId,})
      .then((tasks) => callback(null, tasks))
      .catch((error) => callback(error));
  };

  tasksService.createTask = (data, callback) => {
    Tasks.create(data)
      .then(task => callback(null, task))
      .catch(error => callback(error));
  };

  return tasksService;
};
