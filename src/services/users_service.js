'use strict';

module.exports = function (db) {
  const Users = db.models.Users;
  const usersService = {};

  usersService.findUser = function (id, callback) {
    Users.findById(id)
      .then(user => {
        callback(null, user);
      })
      .catch(error => callback(error));
  };

  usersService.deleteUser = (id, callback) => {
    Users.findOneAndRemove({_id: id,})
      .then(() => {
        callback(null);
      })
      .catch(error => callback(error));
  };

  usersService.createUser = (data, callback) => {
    Users.create(data)
      .then(user => callback(null, user))
      .catch(error => callback(error));
  };

  return usersService;
};
