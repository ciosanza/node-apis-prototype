'use strict';

const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const Schema = mongoose.Schema;

module.exports = function () {
  const taskSchema = new Schema({
    title: {
      type: String,
      required: true,
      index: {
        unique: true,
      },
    },
    done: {
      type: Boolean,
      default: false,
    },
    _creator: {
      type: String,
      ref: 'Users',
    },
  });

  taskSchema.plugin(timestamps);
  return mongoose.model('Tasks', taskSchema);
};
