'use strict';

const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;
const SALT_WORK_FACTOR = 10;

module.exports = function () {
  const userSchema = new Schema({
    username: {
      type: String,
      required: true,
      index: {
        unique: true,
      },
    },
    password: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      index: {
        unique: true,
      },
    },
  });

  // Dont not using arrow function in here
  // because it will make this as undefind
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions
  userSchema.pre('save', function (next) {
    let user = this;
    // Only hash the password if it has ben modified (or is new)
    if (!user.isModified('password')) return next();

    // Generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, (error, salt) => {
      if (error) return next(error);
      // Hash the password using our new salt
      bcrypt.hash(user.password, salt, (error, hash) => {
        if (error) return next(error);
        // Override the clear text password with the hashed once
        user.password = hash;
        next();
      });
    });
  });

  userSchema.methods.comparePassword = function (candidatePassword, callback) {
    let user = this;
    bcrypt.compare(candidatePassword, user.password, (error, isMatch) => {
      if (error) return callback(error);
      callback(null, isMatch);
    });
  };

  userSchema.plugin(timestamps);
  return mongoose.model('Users', userSchema);
};
