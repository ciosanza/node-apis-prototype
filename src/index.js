"use strict";

const Express = require('express');

const diContainer = require('./libs/di_container');

const config = require('./libs/config');
const db = require('./libs/db');
const auth = require('./libs/auth');
const authService = require('./services/auth_service');
const authController = require('./controllers/auth_controller');
const usersService = require('./services/users_service');
const usersController = require('./controllers/users_controller');
const tasksService = require('./services/tasks_service');
const tasksController = require('./controllers/tasks_controller');

const routeIndex = require('./routes/index');
const authRoute = require('./routes/auth_route');
const usersRoute = require('./routes/users_route');
const tasksRoute = require('./routes/tasks_route');
const middlewares = require('./libs/middlewares');
const boot = require('./libs/boot');

let app = module.exports = new Express();

const dic = app.diContainer = diContainer();
dic.register('dbName', 'example-db');
dic.register('tokenSecret', 'SHHH!');
dic.register('app', app);

dic.factory('config', config);
dic.factory('db', db);
dic.factory('auth', auth);
dic.factory('authService', authService);
dic.factory('usersService', usersService);
dic.factory('tasksService', tasksService);
dic.factory('authController', authController);
dic.factory('usersController', usersController);
dic.factory('tasksController', tasksController);


dic.init('middlewares', middlewares);
dic.init('routeIndex', routeIndex);
dic.init('authRoute', authRoute);
dic.init('usersRoute', usersRoute);
dic.init('tasksRoute', tasksRoute);
dic.init('boot', boot);

dic.setup();

