'use strict';

module.exports = (grunt) => {

  // Configuration
  grunt.initConfig({

    // Get the configuration info from package.json ----------------------------
    // This way we can use things like name and version (pkg.name)
    pkg: grunt.file.readJSON('package.json'),

    // Eslint
    eslint: {
      target: [
        'src/**/*.js',],
    },

    // Nodemon
    nodemon: {
      dev: {
        script: 'src/index.js',
        options: {
          ext: 'js,json',
        },
      },
      cluster: {
        script: 'clusters.js',
        options: {
          ext: 'js,json',
        },
      },
    },

    //Concurrent
    concurrent: {
      target: {
        tasks: [
          'nodemon:dev',
          // 'watch',
        ],
        options: {
          logConcurrentOutput: true,
        },
      },
      cluster: {
        tasks: [
          'nodemon:cluster',
          // 'watch',
        ],
        options: {
          logConcurrentOutput: true,
        },
      }
    },

    // Mocha Test
    mochaTest: {
      routes: {
        options: {
          reporter: 'spec',
          slow: 10000,
          timeout: 10000,
        },
        src: ['test/routes/*.js',],
      },
    },

    // start - code coverage settings
    // Environment
    env: {
      dev: {
        NODE_ENV: 'dev',
      },
      test: {
        NODE_ENV: 'test',
      },
      coverage: {
        NODE_ENV: 'test',
        APP_DIR_FOR_CODE_COVERAGE: '../test/coverage/instrument/src/',
        // APP_DIR_FOR_CODE_COVERAGE: '../src/',
      },
    },

    // Clean up
    clean: {
      coverage: {
        src: [
          'test/coverage/',
        ],
      },
    },

    // Copy
    copy: {},

    // Instrument
    instrument: {
      files: [
        'src/**/*.js',
      ],
      options: {
        lazy: true,
        basePath: 'test/coverage/instrument/',
      },
    },

    // Store Coverage
    storeCoverage: {
      options: {
        dir: 'test/coverage/reports',
      },
    },

    // Make report
    makeReport: {
      src: 'test/coverage/reports/**/*.json',
      options: {
        type: 'lcov',
        dir: 'test/coverage/reports',
        print: 'detail',
      },
    },

    // end - code coverage settings

    // api doc
    apidoc: {
      myapp: {
        src: 'src/routes/',
        dest: 'src/public/apidoc/',
      },
    },

    // Sonaqube
    sonarRunner: {
      analysis: {
        options: {
          debug: true,
          separator: '\n',
          sonar: {
            host: {
              url: 'http://192.168.1.20:9000',
            },
            jdbc: {
              url: 'jdbc:h2:tcp://192.168.1.20/sonar',
              username: 'sonar',
              password: 'sonar',
            },

            projectKey: 'node-apis-prototype',
            projectName: 'Node Apis Prototype',
            projectVersion: '0.10',
            sources: ['src', 'test',].join(','),
            language: 'js',
            sourceEncoding: 'UTF-8',
            javascript: {
              lcov: {
                reportPath: 'test/coverage/reports/lcov.info',
              },
            },
            exclusions: [
              'src/public',
              'test/coverage/instrument/src/public',
            ],
          },
        },
      },
    },

  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-istanbul');
  grunt.loadNpmTasks('grunt-env');
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-apidoc');
  grunt.loadNpmTasks('grunt-sonar-runner');

  grunt.registerTask('server', [
    'concurrent:target',
  ]);

  grunt.registerTask('cluster', [
    'concurrent:cluster',
  ]);

  grunt.registerTask('default', [
    'eslint',
    'server',
  ]);

  grunt.registerTask('test', [
    'eslint',
    'env:test',
    'mochaTest:routes',
  ]);

  grunt.registerTask('doc', [
    'apidoc',
  ]);

  grunt.registerTask('coverage', [
    'eslint',
    'clean:coverage',
    'env:coverage',
    'instrument',
    'mochaTest:routes',
    'storeCoverage',
    'makeReport',
    'sonarRunner:analysis',
  ]);

  grunt.registerTask('analysis', [
    'sonarRunner:analysis',
  ]);
};
